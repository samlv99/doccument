### 1. SOLID

SOLID là 1 nguyên lý thiết kế phần mềm quan trọng sử dụng để tạo các hệ thống linh hoạt, dễ bảo trì và mở rộng. Đây là viết tắt của 5 nguyên tắc, mỗi cái đại diện cho 1 nguyên tắc riêng biệt

- S-Single Responsibility Principle (Nguyên tắc trách nhiệm duy nhất): Một lớp chỉ nên có 1 lý do duy nhất để thay đổi. Điều này đảm bảo cho mỗi lớp hoặc module trong hệ thống chỉ chịu trách nhiệm cho 1 nhiệm vụ cụ thể. Nếu 1 lớp chịu nhiều nhiệm vụ nó có thể trở lên khó bảo trì và mở rộng

- O-Open Closed Principle (OCP) (Nguyên tắc mở rộng đóng cửa): Phần mở rộng của hệ thống được thực hiện thông qua việc mở rộng mà không làm thay đổi mã nguồn. Điều đó có nghĩa là các lớp hoặc module phải được thiết kế để mở rộng mà không phải sửa đổi mã nguồn hiện tại

- L-Liskov Substitution Principle (Nguyên tắc thay thế Liskov): Đối với lớp con nó nên thay thế cho lớp cha mà không làm thay đổi tính đúng đắn của chương trình. Điều đó có nghĩa các đối tượng có thể được thay thế bởi các phiên bản của lớp cha mà không làm thay đổi tính nhất quán của chương trình (ví dụ như bạn có 1 lớp cha là lớp vịt, bạn có thể tạo ra instance lớp con kế thừa lớp cha là vịt trời, vịt cỏ, ..., nhưng bạn không thể tạo ra vịt cao su hay vịt đồ chơi kế thừa lớp vịt được, điều đó vi phạm nguyên tắc trên)

- I-Interface Segregation Principle (Nguyên tắc phân tách giao diện): Một giao diện nên được thiết kế chính xác cho các yêu cầu của các client của nó. Điều này đảm bảo rằng các client không cần phải triển khai các phương thức mà họ không sử dụng. (ví dụ những loại động vật có thể bay được có thể tạo ra 1 interface fly animal, những loại động vật có thể đi bộ thì tạo ra interface wall animal, những loại động vật có thể bơi tạo ra swim animal, như con chó thì implement wall animal và swim animal, vì thế chúng ta không nên tạo ra 1 interface chung mà nên tách ra từng cụm, nếu tạo ra 1 interface chung như animal thì con chó không thể bay thì nó trở nên bị dư thừa)

- D-Dependency Inversion Principle (Nguyên tắc đảo ngược phụ thuộc): Các module cấp cao không nên phụ thuộc vào các module cấp thấp mà cả 2 nên phụ thuộc vào abstraction. (ví dụ, thay vì gọi thẳng trực tiếp đến 1 đối tượng cụ thể, chúng ta nên phụ thuộc vào các interface hoặc abstraction --> làm giảm sự phụ thuộc vào các module)


### 2. Event-driven architecture

- 
