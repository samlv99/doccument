const user = {
  age: 31,
  name: "John",
  block: "samlv3112",
};

const isOld = (user) => {
  return user?.age ?? 0 > 30;
};

console.log(isOld(user));

// level 1
if (user.age > 30 && user.block === "samlv3112" && user.name === "John") {
  // todo...
}

// level 2
const isOk =
  user.age > 30 && user.block === "samlv3112" && user.name === "John";

if (isOk) {
  // todo...
}

// level 3

const isOk1 = (user) => {
  return user.age > 30 && user.block === "samlv3112" && user.name === "John";
};

if (isOk1(user)) {
  // todo...
}

const respenseHttpCode = (code) => {
  switch (code) {
    case 100:
      console.log("continue");
      break;
    case 200:
      console.log("success");
      break;
    case 201:
      console.log("create");
      break;
    case 202:
      console.log("202k");
      break;
    case 203:
      console.log("defaksd");
      break;
    case 204:
      console.log("no content");
      break;
    default:
      console.log("unknown code");
      break;
  }
};

// level 2:
const respenseHttpCode1 = new Map([
  ["100", "continue"],
  ["200", "success"],
  ["201", "create"],
  ["202", "202k"],
  ["203", "defaksd"],
  ["204", "no content"],
  ["default", "unknown code"],
]);

const returnHttpCode = (code) => {
  return respenseHttpCode1.get(code) ?? "unknown code";
};

console.log(returnHttpCode("201"));
