### 1. Elastic Search là gì?

- Elastic search là công cụ tìm kiếm và phân tích dữ liệu phân tán mã nguồn mở, được xây dựng trên nền tảng Apache lucene, cho phép tìm kiếm 1 cách nhanh chính xác mạnh mẽ và trong thời gian thực

- Đặc điểm của elastic search:

* Tìm kiếm và phân tích: ES hỗ trợ truy vấn toàn văn, phân tích dưới dạng văn bản một cách nhanh, mạnh mẽ và chính xác
* Phân tán và mở rộng: ES thiết kế để chạy trên 1 cụm máy chủ phân tán. Dữ liệu được chia nhỏ ra và lưu trữ trên nhiều node, giúp tăng khả năng chịu lỗi và mở rộng hệ thống
* RESTFul API: ES sử dụng restful API với phương thức HTTP để tương tác dữ liệu dưới dạng JSON, tích hợp với nhiều hệ thống
* Inverted index: Sử dụng chỉ số đảo ngược, Elasticsearch tối ưu hóa cho việc tìm kiếm văn bản, cho phép truy vấn nhanh chóng và hiệu quả.

### 2. Mapping trong elastic search?

- Mapping trong ES đóng vai trò quan trọng trong việc kiểm soát cách dữ liệu được lưu trữ và tìm kiếm, nó giúp ES phân tích dữ liệu và tìm kiếm 1 cách hiệu quả

- Xác định kiểu dữ liệu: Mapping cho phép bạn định nghĩa rõ kiểu dữ liệu của từng trường như (string, interger, boolean, date). Điều này giúp ES biết kiểu dữ liệu để lưu trữ và tìm kiếm 1 cách hiệu quả

- Đảm bảo tính nhất quán của dữ liệu
