### 1. Websocket là gì?

- Websocket là 1 công nghệ cho phép client có thể giao tiếp song song với server

### 2. Khác nhau giữa UDP và TCP

- Cả 2 đều là giao thức truyền dữ liệu trong môi trường mạng

* Kiểm soát lỗi: TCP có kiểm soát lỗi, nó có cơ chế gửi lại dữ liệu khi bị mất hoặc bị hỏng, đảm bảo cho dữ liệu đến đích 1 cách chính xác, UDP ko kiểm soát lỗi, nếu dữ liệu bị mất hoặc bị hỏng thì nó không có cơ chế gửi lại
* Độ tin cậy: TCP là 1 giao thức đáng tin cậy, nó đảm bảo cho dữ liệu đến đích 1 cách chính xác và theo thứ tự, UDP thì ngược lại phù hợp với những ứng dụng có tốc độ cao mà không cần đảm bảo dữ liệu như stream video, ứng dụng trò chơi thời gian thực

--> Tóm lại thì UDP thích hợp với ứng dụng cần tốc độ cao mà không càn đảm bảo dữ liệu, còn tcp thì thích hợp với ứng dụng cần chính xác và đảm bảo dữ liệu như truyền file

### 3. why use websocket over HTTP?

- 1 kết nối websocket là một kết nối liên tục giữa client và server. Kết nối liên tục đó cho phép những điều sau:

* Dữ liệu có thể được gửi từ server đến client mà không cần client phải gửi request. Điều đó giúp client có được dữ liệu mới mà không cần phải gửi liên tục định kỳ để kiểm tra khi nào có dữ liệu mới từ server. Điều đó kém hiệu quả

* Dữ liệu được gửi 1 trong 2 cách rất hiệu quả vì kết nối đã được thiết lập với websocket thì (mostly 6 extra bytes, 2 bytes for header and 4 bytes for Mask) việc gửi dữ liệu thông qua websocket hiệu quả hơn so với HTTP vì HTTP cần header, cookie, etc

### 4. Khác nhau giữa long polling và short polling?

- Short polling là một kỹ thuật mà client sẽ gửi request đến server sau một khoảng thời gian định kỳ để cập nhật dữ liệu mới

* Ưu điểm: Dễ triển khai, không cần phải giữ kết nối trong thời gian dài
* Nhược điểm: Tốn tài nguyên server vì phải xử lý nhiều request không cần thiết, độ trễ cao giữa 2 lần kiểm tra dữ liệu mới (vì client chỉ có thể cập nhật dữ liệu mới ở lần request tiếp theo)

- Long polling là 1 kỹ thuật được cải tiến từ short polling, client gửi request đến server và được giữ kết nối cho đến khi server có dữ liệu mới gủi cho client

* Ưu điểm: Giảm tải cho server, độ trễ thấp giữa các lần cập nhập dữ liệu
* Nhược điểm: Phức tạp trong việc triển khai, cần thiết lập quản lý kết nối lâu dài tránh timeout
