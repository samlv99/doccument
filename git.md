### 1. khác nhau giữa git fetch và git pull

- git fetch: kéo tất cả các thay đổi mới nhất từ remote repository nhưng không làm thay đổi gì trên working directory, các thay đổi được lưu trong remote tracking branchs (ex: origin/develop)
- git pull: thực hiện 2 hành động git fetch trước rồi sau đó git merge, nó sẽ kéo thay đổi mới nhất từ remote repository rồi sau đó thực hiện hợp nhất với local

### 2. khác nhau giữa rebase và merge

- Cả 2 đều dùng để tích hợp những thay đổi của 1 nhánh vào 1 nhánh khác
- merge sẽ tạo ra 1 merge commit mới để dễ dàng theo dõi điểm hợp nhất giữa 2 nhánh, lịch sử sẽ có dạng nhánh, tránh được conflict
- rebase sẽ không tạo 1 merge commit mới, lịch sử sẽ có dạng line near

### 3. git stash để làm gì

- git stash dùng để lưu trữ những thay đổi chưa được commit trong thư mục làm việc local của bạn. Điều này có thể hữu ích khi bạn muốn chuyển sang 1 nhánh khác để phát triển hay sửa lỗi mà không muốn commit các thay đổi hiện tại
