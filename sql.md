### 1. Khác nhau giữa where và having trong sql

- cả 2 câu lệnh truy vấn đều được sử dụng để lọc dữ liệu, nhưng với mục đích và cách sử dụng khác nhau

* Where: được sử dụng để lọc các bản ghi trước khi nhóm hoặc tổng hợp chúng, được sử dụng với các trường dữ liệu cụ thể để lọc trực tiếp các giá trị bản ghi đó
* Having: được sử dụng để lọc các nhóm dữ liệu kết quả của 1 truy vấn chứa các hàm như SUM, AVG, COUNT,.. Nó chỉ hoạt động khi các dữ liệu được nhóm bởi GROUP BY

### 2. Cho 1 bảng players có các cột id, name, point, tìm các bản ghi có giá trị point lớn thứ 2

`SELECT * FROM players WHERE point = (SELECT DISTINCT point FROM players ORDER BY point DESC LIMIT 1, 1);`

### 3. Cho 1 bảng players có các cột id, name, point, tìm các bản ghi có giá trị point lớn thứ 3

`SELECT * FROM players WHERE point = (SELECT DISTINCT point FROM players ORDER BY point DESC LIMIT 1 OFFSET 2);`

### 4. Cho 1 bảng players có các cột id, name, point, tìm các bản ghi có giá trị point lớn nhất

`SELECT * FROM players WHERE point = (SELECT MAX(point) FROM players);`

### 5. Ràng buộc trong sql

- Primary key: Ràng buộc khóa chính, đảm bảo cho 1 cột hoặc 1 nhóm các cột là duy nhất và không thể null, 1 bảng chỉ có 1 primary key
- unique: giống primary key nhưng cho phép null
- Foreign Key Constraint: ràng buộc khóa ngoại, thường là 1 trường hoặc một nhóm các trường của 1 bảng trỏ đến khóa chính trong bảng khác
- Check Constraint: ràng buộc kiểm tra, ràng buộc này kiểm tra giá trị của 1 cột để đảm bảo nó thỏa mãn 1 điều kiện xác định
- Default Constraint: ràng buộc mặc định, ràng buộc này xác định 1 giá trị mặc định của cột trong trường hợp không có giá trị nào
- Not null: ràng buộc này đảm bảo cho giá trị của cột không được null

### 6. Có bao nhiêu loại join

- Inner join: chỉ trả về các bản ghi có điều kiện kết nối của 2 hoặc nhiều bảng
- Left join: trả về tất cả các bản ghi từ bảng bên trái và những bản ghi từ bảng bên phải thỏa mãn điều kiện nối đã xác định.
- Right join: trả về tất cả các bản ghi từ bảng bên phải và những bản ghi từ bảng bên trái thỏa mãn điều kiện nối đã xác định.
- Full join: trả về tất cả các bản ghi từ cả hai (hoặc tất cả) bảng. Nó có thể được coi là sự kết hợp của các phép nối trái và phải.

### 7. Transaction trong sql

- Trong sql, transaction là 1 tập hợp các lệnh sql được thực thi một cách tuần tự và độc lập. Mục đích của transaction là đảm bảo cho tính toàn vẹn của dữ liệu, tức là dữ liệu phải được nhất quán và không được mất đi trong trong quá trình thực thi

- Các đặc điểm quan trọng của transaction

* Automicity: Mọi lệnh trong transaction phải được thực thi hoặc không được thực thi hoàn toàn, không có trạng thái trung gian
* Consistency: Dữ liệu phải luôn hợp lệ trước và sau khi transaction xảy ra
* Isolation: Transaction không được ảnh hưởng bởi các transaction khác xảy ra đồng thời
* Durability: Khi 1 transaction được hoàn thành, dữ liệu phải được đồng bộ với database kể cả khi hệ thống xảy ra lỗi

### 8. Tìm max salary từ mỗi departerment

`select departerentId, max(salary) from employee group by departmentId`

### 9. In ra tên của các employee khác nhau có ngày sinh nằm trong khoảng thời gian nhất định

`select distinct name from employee where birth between "01/01/1990" and "01/01/1999"`

### 10. Tìm tên của employee có chứa từ "joe" không phân biệt chữ hoa, thường

`select * from employee where UPPER(name) like "%JOE%"`

### 11. Xóa các hàng trùng lặp dựa trên các cột

`delete from employee where id not in (select min(id) from employee group by column1, column2, ...)`

### 12. Các index trong mysql

- Index là cơ chế được sử dụng để tăng tốc độ truy vấn trong mysql. Dưới đây là một số loại index phổ biến:

* Primary key index: đây là loại index dược tạo duy nhất trên cột định danh chính. Nó đảm bảo cho giá trị không bị trùng lặp trên cột định danh và giúp tìm kiếm nhanh chóng theo giá trị của cột định danh chính
* Unique index: đây cũng là loại index duy nhất, nó đảm bảo cho giá trị của cột hoặc một nhóm các cột không bị trùng lặp
* Index: Dây là loại index phổ biến, nó giúp tạo cấu trúc dữ liệu tìm kiếm nhanh chóng dựa trên giá trị của cột, khi tạo index có thể tạo trên 1 cột hoặc 1 nhóm cột
* Fulltext index: loại index naỳ được sử dụng cho các truy vấn tìm kiếm văn bản đầy đủ

### 13. Deadlock trong mysql

- Deadlock trong mysql xảy ra khi 2 hoặc nhiều transaction cùng tồn tại và cùng yêu cầu khóa 1 tài nguyên, tạo ra 1 chu kỳ phụ thuộc
- ví dụ:
  `Transaction #1
START TRANSACTION;
UPDATE orders SET price = 50 WHERE id = 2;
UPDATE orders SET price = 60 WHERE id = 6;
COMMIT;`

`Transaction #2
START TRANSACTION;
UPDATE orders SET price = 60 WHERE id = 6;
UPDATE orders SET price = 50 WHERE id = 2;
COMMIT;`

- Xử lý:

* Giảm thời gian mà các transaction lock bằng cách set "lock wait timeout" và xử dụng try catch
* Giữ cho transacion càng ngắn càng tốt để giảm khả năng deadlock.
* Đảm bảo các transaction khóa bảng theo một thứ tự nhất định. Điều này tránh transaction A chờ transaction B và ngược lại

### 14. N+1 trong mysql

- Ví dụ ta có 1 bảng user và mỗi người dùng có 1 số bản ghi liên quan đến bảng post, khi ta muốn lấy tất cả user và các bài đăng liên quan đến user đó thì ta viết 1 truy vấn chính để lấy toàn bộ user, sau đó mỗi 1 user bạn lại dùng 1 truy vấn phụ để lấy ra toàn bộ bài đăng của user đó --> vấn đề hiệu suất nếu số lượng bản ghi lớn
- Để giải quyết vấn đề trên ta có thể dùng 1 số kỹ thuật như JOIN hoặc subquery để nhóm những thông tin từ nhiều bảng trong 1 câu query
- VD: `SELECT Users.id, Users.name, COUNT(Posts.id) AS post_count FROM Users LEFT JOIN Posts ON Users.id = Posts.user_id GROUP BY Users.id;`

### 15. Pessimistic vs. Optimistic Locking trong mysql

- Pessimistic và Optimistic locking là 2 chiến lược khóa dữ liệu phổ biến được sử dụng để quản lý truy cập đồng thời trong các cơ sở dữ liệu

- Pessimistic locking: Khi 1 phiên làm việc cần cập nhật bản ghi, nó sẽ khóa bản ghi này ngay lập tức và sẽ mở khi kêt thúc phiên làm việc hoàn tất cho việc cập nhật và transaction kết thúc --> điều đó tránh được các phiên làm việc khác cập nhật bản ghi này và tránh được xung đột dữ liệu

* Ưu điểm: Giảm thiểu khả năng xung đột, đảm bảo dữ liệu được nhất quán, thích hợp với ứng dụng có nhiều xung đột và các hệ thống có nhiều cập nhập dữ liệu

* Nhược điểm: Có thể gây ra tình trạng chờ đợi lâu cho các ứng dụng có nhiều lượng truy cập đồng thời vì các bản ghi bị khóa không được truy cập bởi các phiên làm việc khác

- Optimistic locking: Thay vì khóa bản ghi ngay lập tức thì phiên truy cập sẽ đọc bản ghi. Khi phiên làm việc muốn cập nhật bản ghi thì sẽ kiểm tra xem nó đã bị cập nhật bởi phiên làm việc khác chưa. Nếu bản ghi đã bị thay đổi thì phiên làm việc có thể thông báo lỗi

* Ưu điểm: Tăng khả năng đồng thời vì không khóa bản ghi trong quá trình thực hiện, thích hợp với những ứng dụng có lượng cập nhật đồng thời thấp ít xung đột

* Nhược điểm: Cần xử lý xung đột khi phát hiện bản ghi bị thay đổi

--> Pessimistic locking thích hợp với những hệ thống có nhiều xung đột và tính nhất quán dữ liệu còn optimistic locking thì thích hợp với những hệ thống ít xung đột và tính đồng thời cao

### 16. Difference between CHAR and VARCHAR data types?

- CHAR: đại diện cho 1 chuỗi có độ dài cố định. Nếu độ dài của dữ liệu nhỏ hơn độ dài cố định của char thì nó sẽ chèn khoảng trống vào
- VARCHAR: đại diện cho 1 chuỗi có độ dài thay đổi và nó được chèn vào phụ thuộc vào độ dài của chuỗi. nó truy cập chậm hơn so với char vì có độ dài không cố định

### 17. Difference between delete and truncate

- Delete trong sql là câu lệnh để xóa 1 hoặc nhiều hàng trong 1 bảng
- Truncate là câu lệnh trong sql để xóa toàn bộ các bản ghi trong bảng