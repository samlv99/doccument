### 1. Message queue là gì?

- Message queue là một thành phần quan trọng thường dùng trong các hệ thống lớn, hoặc phần mềm theo kiến trúc microservice
- Message queue cho phép các thành phần service trong một hệ thống trao đổi thông tin cho nhau
- Các thành phần của hệ thống Message queue:

* Message: Thông tin được gửi (có thể là text, binary hoặc JSON)
* Message Queue: Nơi chứa những message này, cho phép producer và consumer có thể trao đổi với nhau
* Producer: Service tạo thông tin, đưa thông tin vào message queue
* Consumer: Service nhận message từ message queue và xử lý
* Một service có thể vừa làm producer vừa làm consumer

- Một số message queue được dùng hiện nay: Kafka, RabitMQ, etc
  ![alt text](./image/image.png)
