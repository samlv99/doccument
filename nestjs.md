### 1. Guards, Middleware, Pipes trong nestjs?

- Guards:

* Trong nestjs, guards là 1 tính năng mạnh mẽ để kiểm soát quyền truy cập vào các route và các phương thức. Nó hoạt động bằng cách xử lý logic trước khi route handle được gọi. Chúng thường được dùng để xác thực hoặc phân quyền các request HTTP

* Chúng thường được áp dụng ở cấp độ toàn bộ ứng dụng, module, controller, hoặc phương thức cụ thể

- Middelware:

* Middelware trong nestjs là thành phần trung gian được dùng để xử lý các yêu cầu HTTP trước khi chúng được chuyển đến các handler. Nó thường được dùng để xác thực, phân quyền, ghi log hoặc xử lý lỗi

- Pipes:

* Pipes trong nestjs có mục đích biến đổi và xác thực dữ liệu, nó cũng được sử dụng ở nhiều cấp độ toàn bộ hệ thống, module, controller hoặc phương thức cụ thể, chúng thường được sử đụng để biến đổi dữ liệu thành dạng mong muốn và kiểm tra tính hợp lệ của dữ liệu đó

### 2. Interceptor trong nestjs? Khác nhau giữa Interceptor và Middelware?

- Interceptor trong nestjs là một tính năng mạnh mẽ dùng để can thiệp vào quá trình xử lý HTTP. Nó được dùng để xử lý thay đổi dữ liệu của request và response hoặc có thể dùng để ghi log hoặc xử lý ngoại lệ

- Middelware và interceptor khác nhau về cách hoạt động và mục đích sử dụng

* Middelware:

- Thực thi trước router: Middelware thường đứng trước router để xử lý các yêu cầu
- Sử dụng ở cấp độ toàn ứng dụng hoặc module
- Không can thiệp vào response: Middelware không can thiệp hoặc biến đổi dữ liệu response sau khi handle của route xử lý
  --> Thường xử lý chung cho mọi request trước khi chúng đến route handler như xác thực, phân quyền, ghi log

* Interceptor

- Thực thi trước và sau route handler
- Biến đổi dữ liệu của cả request và response
- Sử dụng cho toàn ứng dụng controller hoặc module
  --> Sử dụng khi bạn muốn thay đổi dữ liệu request hoặc response thao tác trước và sau route handle
