### 1. Nodejs là gì và tại sao nó lại quan trọng trong việc phát triển web?

Nodejs là một môi trường chạy mã JavaScript ở phía máy chủ (server-side) được xây dựng trên javascript engine của Chrome(V8 engine). Nó cho phép bạn thực thi mã javascript không chỉ trên trình duyệt web mà nó còn trên máy chủ, giúp phát triển ứng dụng web và ứng dụng mạng hiệu quả hơn

Lý do tại sao Nodejs quan trọng trong việc phát triển web:

- Sử dụng cùng ngôn ngữ: Nodejs cho phép nhà phát triển cả phía máy chủ và máy khách bằng cùng ngôn ngữ lập trình (javascript) điều này giúp giảm độ phức tạp trong việc quản lý mã nguồn
- Non-blocking và asynchronous: Nodejs được thiết kế làm việc theo mô hình non-blocking, có nghĩa là nó có khả năng xử lý hàng loạt yêu cầu mà không cần chờ đợi một yêu cầu hoàn thành trước khi xử lý yêu cầu khác. Điều này làm cho ứng dụng nodejs trở nên hiệu quả và nhanh chóng trong việc xử lý nhiều yêu cầu cùng một lú, đặc biệt là trong ứng dụng web thời gian thực
- Hệ sinh thái mạnh mẽ: Nodejs có một cộng đồng phát triển mạnh mẽ và một hệ sinh thái đa dạng của các thư viện framework bổ trợ, các thư viện như expressjs, socket.io và mongoose giúp phát triển ứng dụng web dễ dàng hơn
- Tốc độ cao: Nhờ sử dụng JS Engine v8 của chrome nên nodejs thường có hiệu xuất và tốc độ nhanh. Điều này làm cho nó trở thành một lựa chọn hấp dẫn cho việc xây dựng các ứng dụng có yêu cầu xử lý nhanh, như ứng dụng thời gian thực hoặc ứng dụng có lưu lượng truy cập lớn.
- Dễ dàng mở rộng: Nodejs cho phép mở rộng dễ dàng bằng cách xử dụng cơ chế non-blocking và hỗ trợ các ứng dụng phân tán. Điều này giúp ứng dụng có thể mở rộng để đáp ứng nhu cầu tăng lưu lượng truy cập mà khồn cần thay đổi quá nhiều mã nguồn

* Cơ chế non-blocking là một phương pháp xử lý các tác vụ không chặn luồng thực hiện của chương trình. Nó cho phép chương trình tiếp tục thực hiện các tác vụ khác mà không phải chờ tác vụ hiện tại hoàn thành trước

### 2. Ưu và nhược điểm của nodejs

- Ưu điểm:

* Hiệu suất cao: Node.js được xây dựng trên JavaScript Engine V8 của Chrome, có hiệu suất tốt, cho phép xử lý hàng loạt yêu cầu mạng cùng lúc và đáp ứng nhanh hơn so với một số môi trường khác.
* Kiến trúc non-blocking và event-driven: Nodejs sử dụng mô hình non-blocking, cho phép xử lý đồng thời nhiều yêu cầu mà không cần phải chờ đợi hoàn thành, giúp tối ưu sử dụng tài nguyên hệ thống và làm cho ứng dụng nhanh chóng
* Dễ dàng mở rộng: Nodejs hỗ trợ mở rộng dễ dàng bằng cách sử dụng các module cluster hoặc pm2 để xử lý tải cao và tạo ra nhiều phiên bản sao chép của ứng dụng
* Đồng nhất ngôn ngữ lập trình: Nodejs cho phép chúng sử dụng cùng một ngôn ngữ js cho cả phía server và client --> giảm độ phức tạp trong quản lý mã nguồn

- Nhược điểm:

* Chưa thích hợp cho các tác vụ CPU-bound: Node không phù hợp cho các tác vụ CPU-bound (tác vụ đòi hỏi sử dụng CPU mạnh mẽ) do kiến trúc non-blocking của nó. Việc thực hiện các tác vụ tính toán phức tạp có thể làm chậm toàn bộ ứng dụng
* Khó quản lý cho các ứng dụng lớn và phức tạp: Node không cung cấp cơ chế module hóa tự nhiên nên việc quản lý và scale trở lên khó khăn
* Xử lý bất đồng bộ --> mã trở lên khó đọc

### 3. Giải thích vòng đời của một yêu cầu trong NodeJs

- Request: khi có một yêu cầu từ client tới server, quá trình vòng đời bắt đầu. Yêu cầu này bao gồm HTTP method, header, và dữ liệu request có thể là param hoặc body
- Routing: Sau khi server nhận yêu cầu, nó sẽ phân tích yêu cầu xem yêu cầu đó cần được xử lý bởi đoạn mã nào.
- Middleware processing: thực hiện các tác vụ như xác thực, ghi log, kiểm tra lỗi và các tác vụ xử lý khác
- Request handling: Sau khi đi qua middleware, yêu cầu được chuyển đến hàm xử lý chính, đây là nơi thực hiện các tác vụ cụ thể tương ứng với yêu cầu như truy xuất dữ liệu từ database, xử lý logic hoặc tạo phản hồi.
- Response: Sau khi xử lý yêu cầu, server tạo response dựa trên kết quả xử lý, Phản hồi bao gồm HTTP status code, header và dữ liệu trả về cho client

### 4. Nodejs có hỗ trợ đa luồng không? Nếu không thì làm cách nào để nó xử lý các yêu cầu đồng thời?

Nodejs được xây dựng trên kiến trúc đơn luồng, điều này có nghĩa là nó xử dụng 1 luồng duy nhất để xử lý các yêu cầu. Tuy nhiên, node sử dụng mô hình bất đồng bộ để và sự kiện để xử lý các yêu cầu một cách đồng thời

- Bất đồng bộ: Nodejs sử dụng các phương thức không đồng bộ để xử lý I/O, điều này giúp tránh tình trạng block và tiếp tục xử lý các yêu cầu khác trong khi đợi các tác vụ I/O hoàn thành
- Sự kiện: Nodejs sử dụng mô hình event loop để xử lý các yêu cầu một cách bất đồng bộ , mọi tác vụ không đồng bộ được đưa vào hàng đợi và xử lý bởi event loop khi chúng hoàn thành
- child process: nodejs cung cấp khả năng tạo child process để xử lý các tác vụ cần nhiều tài nguyên hoặc đòi hỏi sự đa luồng. Việc này cho phép Nodejs mở rộng để xử ký các yêu cầu đồng thời mà không bị chặn bởi luồng duy nhất

### 5. About Process and Threads?

- Process:

* Là một chương trình độc lập được thực thi có không gian riêng
* Các tiến trình không chia sẻ bộ nhớ với nhau trừ khi nó được chia sẻ thông qua 1 cơ chế giao tiếp giữa các tiến trình
* Các tiến trình độc lập với nhau, sự cố của tiến trình này không ảnh hưởng đến các tiến trình khác
* Ví dụ về process: Có 2 ứng dụng độc lập với nhau, một trình duyệt web (chrome), 1 trình xử lý văn bản (Microsoft word).

- Khi bạn mở chrome thì 1 process được tạo ra, quá trình này có bộ nhớ riêng và không chia sẻ tài nguyên với process khác
- Khi bạn mở microsoft word thì 1 process mới cũng tạo ra, process này cũng có bộ nhớ riêng và không chia sẻ với process khác
- Khi chrome gặp lỗi thì microsoft word vẫn hoạt động bình thường không bị ảnh hưởng

- Thread:

* Luồng là đơn vị thực thi nhỏ nhất bên trong mỗi tiến trình. Các luồng có thể tồn tại bên trong của 1 tiến trình và chia sẻ chung bộ nhớ với nhau
* Các luồng bị cô lập với nhau, sự cố của 1 luồng có thể ảnh hưởng đến cả 1 tiến trình
* Các luồng được chia sẻ bộ nhớ với nhau, điều đó giúp các luồng có thể chia sẻ dữ liệu với nhau một cách dễ dàng
* Ví dụ về thread: khi sử dụng trình duyệt chrome, tab 1 của chrome: tạo ra 1 thread, tab 2 của chrome: tạo ra 1 thread
  --> 2 thread này chia sẻ chung 1 bộ nhớ nên có thể dễ dàng trao đổi dữ liệu giữa các tab của trình duyệt. Nếu tab 1 của trình duyệt bị lỗi có thể sẽ ảnh hưởng đến toàn bộ trình duyệt của chrome

### 6. Worker thread

- Worker thread là 1 module của nodejs cho phép bạn chạy mã JS song song với luồng chính. Sử dụng worker thread khi bạn có 1 công việc đồng bộ tốn nhiều thời gian xử lý, bằng cách này bạn có thể giải phóng luồng chính mà không phải tốn 1 khoảng thời gian nhất định.

### 7. Vì sao phải sử dụng queue trong nodejs, sử dụng queue trong những trường hợp nào?

- Nodejs hoạt động dựa trên mô hình non-blocking, sử dụng single-thread và event loop. Điều này giúp nodejs sử lý các tác vụ đồng thời 1 cách hiệu quả, tuy nhiên việc xử lý các tác vụ nặng trong luồng chính có thể gây ra vấn đề blocking ảnh hưởng đến hiệu suất của các tác vụ khác. Queue sinh ra để giải quyết vấn đề này bằng cách chuyển các tác vụ nặng này sang một luồng riêng và giải phóng luồng chính để xử lý các tác vụ khác

- Trường hợp sử dụng queue: send mail, xử lý ảnh, cập nhật dữ liệu, xử lý các tác vụ cron,...
