### 1. Docker là gì?

- Docker là 1 nền tảng mã nguồn mở giúp triển khai và quản lý các ứng dụng bên trong container. Container giúp đóng gói tất cả các yếu tố để chạy ứng dụng bao gồm mã nguồn, thư viện, phụ thuộc, các tệp cấu hình --> có thể chạy trên bất kỳ môi trường nào bất kỳ nền tảng Mac, window, ubuntu, ...

- Docker images: là các mẫu chỉ định để tạo container. một image bao gồm mọi thứ cần thiết để chạy ứng dụng
- Docker container: là các instance của docker image. Container là các môi trường riêng biệt mà ứng dụng có thể chạy bên trong
- Docker compose: là một công cụ để định nghĩa và chạy các ứng dụng docker đa container, sử dụng tệp yaml để cấu hình các service, network và volumes của ứng dụng

### 2. Phân biệt docker và máy ảo?

- Docker:

* Chia sẻ cùng một kernel với hệ điều hành của host
* Về kích thước thì container thường nhẹ và chiếm ít tài nguyên hơn vì chúng không cần bao gồm 1 hđh đầy đủ
* Về khởi động thì container có thể khởi động rất nhanh chỉ trong vài giây vì chúng không cần khởi 1 hđh hoàn chỉnh
* Về Isolation thì container chạy trên cùng 1 hđt và sử dụng các tính năng như namespace và cgroup để cô lập môi trường

--> Docker sử dụng tài nguyên của hệ thống hiệu quả hơn. Hiệu suất gắn liền với hiệu suất gôc của hđh gốc, thích hợp để chạy nhiều instance của 1 ứng dụng hoặc microservice

- Máy ảo:

* Mỗi VM chạy trên 1 hđh riêng biệt
* Vm thường nặng hơn và chiếm nhiều tài nguyên hơn vì chúng bao gồm toàn bộ hđh cùng với ứng dụng
* Vm cần khởi động hđh đầy đủ --> thời gian khởi động thường lâu hơn, có thể mất vài phút
* Isolation thì Vm hoàn toàn tách biệt với nhau, vì mỗi VM có hđh riêng và toàn bộ tài nguyên hệ thống ảo hóa

--> VMs cần nhiều tài nguyên hơn để chạy mỗi instance do cần chạy hđh riêng, hiệu suất có thể bị giảm

Ví dụ: - Docker giống như sống trong 1 khu chung cư, các căn hộ chia sẻ hạ tầng và tài nguyên, dễ xây dựng và khởi động nhanh chóng - Máy ảo: giống như sống trong 1 ngôi nhà riêng, mỗi ngôi nhà có hạ tầng và tài nguyên riêng, tốn kém và mất time để xây dựng và khởi động
